import * as React from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import { ApplicationState }  from '../store';
import * as LeaveTypesState from '../store/LeaveTypes';

type LeaveTypeProps =
    LeaveTypesState.LeaveTypesState     
    & typeof LeaveTypesState.actionCreators
    & RouteComponentProps<{ }>;

class LeaveTypeData extends React.Component<LeaveTypeProps, {}> {
    componentWillMount() {
        this.props.requestLeaveTypes();
    }

    componentWillReceiveProps(nextProps: LeaveTypeProps) {
        this.props.requestLeaveTypes();
    }

    public render() {
        return <div>
            <h1>Leave Types</h1>
            { this.renderLeaveTypesTable() }
        </div>;
    }

    private renderLeaveTypesTable() {
        return <table className='table'>
            <thead>
                <tr>
                    <th>Code</th>
                    <th>Title</th>
                    <th>Description</th>
                </tr>
            </thead>
            <tbody>
            {this.props.LeaveTypes.map(leaveType =>
                <tr key={leaveType.code}>
                    <td>{leaveType.code}</td>
                    <td>{leaveType.title }</td>
                    <td>{leaveType.description }</td>
                </tr>
            )}
            </tbody>
        </table>;
    }
}

export default connect(
    (state: ApplicationState) => state.leaveTypes,
    LeaveTypesState.actionCreators       
)(LeaveTypeData) as typeof LeaveTypeData;

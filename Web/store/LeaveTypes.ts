import { fetch, addTask } from 'domain-task';
import { Action, Reducer, ActionCreator } from 'redux';
import { AppThunkAction } from './';

export interface LeaveTypesState {
    isLoading: boolean;
    LeaveTypes: LeaveType[];
}

export interface LeaveType {
    code: string;
    title: string;
    description: string;
}

interface RequestLeaveTypesAction {
    type: 'REQUEST_LEAVETYPES';
}

interface ReceiveLeaveTypesAction {
    type: 'RECEIVE_LEAVETYPES';
    LeaveTypes: LeaveType[];
}

type KnownAction = RequestLeaveTypesAction | ReceiveLeaveTypesAction;

export const actionCreators = {
    requestLeaveTypes: (): AppThunkAction<KnownAction> => (dispatch, getState) => {

        let fetchTask = fetch(`api/Leave/Types`)
            .then(response => response.json() as Promise<LeaveType[]>)
            .then(data => {
                dispatch({ type: 'RECEIVE_LEAVETYPES', LeaveTypes: data });
            });

        addTask(fetchTask); 
        dispatch({ type: 'REQUEST_LEAVETYPES' });
    }
};

const unloadedState: LeaveTypesState = { LeaveTypes: [], isLoading: false };

export const reducer: Reducer<LeaveTypesState> = (state: LeaveTypesState, incomingAction: Action) => {
    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'REQUEST_LEAVETYPES':
            return {
                LeaveTypes: state.LeaveTypes,
                isLoading: true
            };
        case 'RECEIVE_LEAVETYPES':
            return {
                LeaveTypes: action.LeaveTypes,
                isLoading: false
            };
        default:
            const exhaustiveCheck: never = action;
    }

    return state || unloadedState;
};

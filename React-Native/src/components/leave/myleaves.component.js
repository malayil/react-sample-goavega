import React, {Component} from "react";
import {View, AsyncStorage} from 'react-native';
import {
  Button,
  Text,
  Container,
  Card,
  CardItem,
  Body,
  Content,
  Header,
  Title,
  Left,
  Icon,
  Right,
  Fab,
  List,
  ListItem,Col,Row,Grid
} from "native-base";
import LeaveService from '../api/leave.service';
import { ActivityIndicator } from 'react-native';
import styles from '../styles/styles.component';
import Moment from 'moment';

export default class MyLeavesComponent extends Component {  
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: false
    };
  };
  async loadData() {
    this.setState({loading: true});
    const api = new LeaveService();
    var response = await api.myLeaves();
    this.setState({loading: false});
    this.setState({data: response.data});
  }
  async componentWillReceiveProps() {
    await this.loadData();
  }
  async componentDidMount() {
    await this.loadData();
  };

  render() {
    Moment.locale('en');    
    let leaveList = this.state.data.map(function (leavedata, index) {
        if(leavedata.status == 'Approved') {
          leavedata.bgcolor = "#00808a";
          leavedata.icon = 'checkmark';
          leavedata.bordercolor = "#00808a";
        }
        if(leavedata.status == 'Rejected') {
          leavedata.bgcolor = "#a2083d";
          leavedata.icon = 'close';
          leavedata.bordercolor = "#a2083d";
        }
        if(leavedata.status == 'Open') {
          leavedata.bgcolor = '#ffc107';
          leavedata.icon = 'clock';
          leavedata.bordercolor = "#ffc107";
        }
        return (
          <Content key={index}>
            <Grid style={{borderWidth:1,borderColor:leavedata.bordercolor,marginBottom:10}}>  
              <Col style={{padding:5,backgroundColor:leavedata.bgcolor}}>
                  <Row style={styles.justifyContent}>
                    <Text style={{color:'white',fontSize:30}}>{leavedata.days}</Text>                      
                  </Row>
                  <Row style={styles.justifyContent}>                     
                    <Text  style={styles.whiteText}>Days</Text>
                  </Row>
                  <Row style={styles.justifyContent}>
                    <Icon name={leavedata.icon}  style={styles.whiteText}/>
                  </Row>
              </Col>
              <Col style={{flex:3,padding:10}}>
                <Row>
                  <Col style={{flex:3,justifyContent:'center'}}>
                    <Text>From</Text>
                    <Text>{Moment(leavedata.fromDate).format('DD MMMM')}</Text>
                  </Col>
                  <Col style={{flex:2,alignItems:'center',justifyContent:'center'}}>
                    <Icon ios='ios-repeat' android="md-swap" />
                  </Col>
                  <Col style={{flex:3,justifyContent:'center'}}>
                    <Text>To</Text>
                    <Text>{Moment(leavedata.toDate).format('DD MMMM')}</Text>
                  </Col>
                </Row>
                <Row> 
                  <Text style={{textAlign:'left',flex:1}}>{leavedata.leaveType}</Text>
                  <Text style={{textAlign:'right',flex:1}}>{leavedata.status}</Text>
                </Row>
              </Col>
            </Grid>
          </Content>
        )
      });
      return (
        <Container>
          <Content padder>
            {leaveList}
            <ActivityIndicator animating={this.state.loading} size="large" color='black'/>
          </Content>
        </Container>
      );
    }
  };